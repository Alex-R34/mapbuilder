using MapBuilder.App;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace MapBuilder.Tests
{
    public class MapBuilderTests
    {
        [Fact]
        public void BuildRouteMap_Case1()
        {
            // Arrange
            App.MapBuilder _mapBuilder = new App.MapBuilder();

            var expectedMap = new[] { "E2", "S", "E2", "E1" };
            var signatureMap = new HashSet<SignStep>
            {
                new() { Floor = 5, Section = 1 },
                new() { Floor = 4, Section = 2 },
                new() { Floor = 1, Section = 2 },
                new() { Floor = 4, Section = 1 }
            };

            // Act
            var actualMap = _mapBuilder.BuildRouteMap(signatureMap).ToList();

            // Assert
            Assert.Equal(expectedMap[0], actualMap[0]);
            Assert.Equal(expectedMap[1], actualMap[1]);
            Assert.Equal(expectedMap[2], actualMap[2]);
            Assert.Equal(expectedMap[3], actualMap[3]);
        }

        [Fact]
        public void BuildRouteMap_Case2()
        {
            // Arrange
            App.MapBuilder _mapBuilder = new App.MapBuilder();

            var expectedMap = new[] { "E1", "S", "S", "E2" };
            var signatureMap = new HashSet<SignStep>
            {
                new() { Floor = 4, Section = 1 },
                new() { Floor = 3, Section = 1 },
                new() { Floor = 5, Section = 2 },
                new() { Floor = 1, Section = 1 }
            };

            // Act
            var actualMap = _mapBuilder.BuildRouteMap(signatureMap).ToList();

            // Assert
            Assert.Equal(expectedMap[0], actualMap[0]);
            Assert.Equal(expectedMap[1], actualMap[1]);
            Assert.Equal(expectedMap[2], actualMap[2]);
            Assert.Equal(expectedMap[3], actualMap[3]);
        }

        [Fact]
        public void BuildRouteMap_Case3()
        {
            // Arrange
            App.MapBuilder _mapBuilder = new App.MapBuilder();

            var expectedMap = new[] { "E2", "S", "S", "S" };
            var signatureMap = new HashSet<SignStep>
            {
                new() { Floor = 5, Section = 1 },
                new() { Floor = 4, Section = 2 },
                new() { Floor = 3, Section = 2 },
                new() { Floor = 1, Section = 1 }
            };

            //Act
            var actualMap = _mapBuilder.BuildRouteMap(signatureMap).ToList();

            //Assert
            Assert.Equal(expectedMap[0], actualMap[0]);
            Assert.Equal(expectedMap[1], actualMap[1]);
            Assert.Equal(expectedMap[2], actualMap[2]);
            Assert.Equal(expectedMap[3], actualMap[3]);
        }

        [Fact]
        public void BuildRouteMap_Case4()
        {
            // Arrange
            App.MapBuilder _mapBuilder = new App.MapBuilder();

            var expectedMap = new[] { "S", "S", "E1", "E2" };
            var signatureMap = new HashSet<SignStep>
            {
                new() { Floor = 2, Section = 2 },
                new() { Floor = 4, Section = 1 },
                new() { Floor = 2, Section = 1 },
                new() { Floor = 5, Section = 2 }
            };

            // Act
            var actualMap = _mapBuilder.BuildRouteMap(signatureMap).ToList();

            // Assert
            Assert.Equal(expectedMap[0], actualMap[0]);
            Assert.Equal(expectedMap[1], actualMap[1]);
            Assert.Equal(expectedMap[2], actualMap[2]);
            Assert.Equal(expectedMap[3], actualMap[3]);
        }

    }
}