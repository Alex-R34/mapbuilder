﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapBuilder.App
{
    public static class IntegerExtensions
    {
        public static bool IsOdd(this int number)
        {
            return number % 2 != 0;
        }
    }
}
