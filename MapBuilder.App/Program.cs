﻿

using MapBuilder.App;

MapBuilder.App.MapBuilder _mapBuilder = new MapBuilder.App.MapBuilder();

var expectedMap = new[] { "E2", "S", "E2", "E1" };
var signatureMap = new HashSet<SignStep>
            {
                new() { Floor = 5, Section = 1 },
                new() { Floor = 4, Section = 2 },
                new() { Floor = 1, Section = 2 },
                new() { Floor = 4, Section = 1 }
            };

var actualMap = _mapBuilder.BuildRouteMap(signatureMap).ToList();

Console.ReadLine();