﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapBuilder.App
{
    public class MapBuilder
    {
        private readonly int _elevatorSectionChangeTime;
        private readonly int _elevatorWaitTime;
        private readonly int _floorStairsTime;
        private readonly int _floorElevatorTime;

        private SignStep _currentOfficeManagerPosition = new()
        {
            Floor = 0,
            Section = 1
        };

        public MapBuilder(int elevatorSectionChangeTime = 1, int elevatorWaitTime = 1, int floorStairsTime = 2, int floorElevatorTime = 1)
        {
            _elevatorSectionChangeTime = elevatorSectionChangeTime;
            _elevatorWaitTime = elevatorWaitTime;
            _floorStairsTime = floorStairsTime;
            _floorElevatorTime = floorElevatorTime;
        }

        public IEnumerable<string> BuildRouteMap(IEnumerable<SignStep> signatureMap)
        {
            List<string> fastestTrip = new List<string>();

            foreach (SignStep step in signatureMap)
            {
                var result = FastestPath(step);
                fastestTrip.Add(result);
                _currentOfficeManagerPosition.Section = step.Section;
                _currentOfficeManagerPosition.Floor = step.Floor;
            }
            return fastestTrip;
        }

        private string FastestPath(SignStep destination)
        {
            var stairsTime = StairsTime(destination);
            var elevatorTime = ElevatorTime(destination);

            if (stairsTime <= elevatorTime)
            {
                return "S";
            }
            else
            {
                return "E" + (destination.Floor.IsOdd() ? "2" : "1");
            }

            throw new Exception("Something went wrong!");
        }

        private int StairsTime(SignStep destination)
        {
            int floorsBetween = Math.Abs(_currentOfficeManagerPosition.Floor - destination.Floor);

            return floorsBetween * _floorStairsTime;
        }

        private int ElevatorTime(SignStep destination)
        {
            var floorsBetween = Math.Abs(_currentOfficeManagerPosition.Floor - destination.Floor);

            return EnterElevatorTime(destination) + ExitElevatorTime(destination) + floorsBetween * _floorElevatorTime;
        }
        private int EnterElevatorTime(SignStep destination)
        {
            if ((destination.Floor + _currentOfficeManagerPosition.Section).IsOdd())
            {
                return _elevatorWaitTime;
            }
            return _elevatorWaitTime + _elevatorSectionChangeTime;
        }

        private int ExitElevatorTime(SignStep destination)
        {
            return (destination.Floor + destination.Section).IsOdd() ? 0 : _elevatorSectionChangeTime;
        }

    }

}
