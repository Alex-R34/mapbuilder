﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapBuilder.App
{
    public struct SignStep
    {
        public int Floor { get; set; }
        public int Section { get; set; }
    }
}
